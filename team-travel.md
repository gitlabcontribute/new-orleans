## Details for the GitLab team travel

This file will **only** apply to the GitLab Team members as **our guests will be responsible for their own travel & visa arrangements**

## Dates and travel:

**Address for visa:**
Hyatt Regency Downtown
601 Loyola Avenue 
New Orleans, Louisiana, United States, 70113

- **Arrival** to the hotel on May 8th at any time throughout the day (this could mean leaving your home a day before)
- **Departure** from the hotel on May 14th at any time throughout the day (no meals included after breakfast)
- **Note** Regular check-in/check-out times apply. You're welcome to extend your stay, at your own expense.


### Booking flights to MSY - Deadline Feb 1st 2019

**Managers, please take note**, you will receive approval notices to approve flights within the budgets below. Only Economy flight bookings are allowed.


* Team members in **the US** have a max budget of $325
* Team members in **EMEA** have a max budget of $1000
* Team members in **APAC** have a max budget of $1300
* Team members in **LATAM** have a max budget of $1200

In NexTravel, set the currency to USD for easy navigation with the budgets above.



* _Before booking_ your flights, make sure your **passport expires after 2019-12-01**
* _Before booking_ your flights, make sure to check what kind of visa you need under the [US Travel Regulations](https://travel.state.gov/content/travel/en/us-visas/tourism-visit/visa-waiver-program.html)
* _Before booking_ ensure that you do not need a transit visa for any layovers, hold off on booking until you checked this and talked with PeopleOps


#### Additional travel info

* If you can't find a flight within the budget, please reach out to travel@gitlab.com _before booking_ to discuss the best and closest option you found, include a screenshot and cc your manager.
* Double check connection times, **entering the US requires a layover of 3 hours** to clear customs and check back in for your next flight!
* Travel budgets include all travel costs, e.g. upgrades, layovers etc
* Details on upgrading flights in [the handbook](https://about.gitlab.com/handbook/spending-company-money/)
* Check flights through apps/sites like [Hopper](https://www.hopper.com/) (great app), [Momondo](https://www.momondo.com/), [Skyscanner](https://www.skyscanner.com), [Hipmunk](https://www.hipmunk.com), or [Kiwi.com](https://kiwi.com) (use discount code `KIWI10`)
* Setup flight alerts for your route to get updates on pricing
* You can book your travel using our travel tool [NexTravel](https://about.gitlab.com/handbook/travel/#booking-travel-through-nextravel-)

### Visa arrangements

After checking if you either need an ESTA or arrange a visa, make sure to make the arrangements as soon as possible.
Check with US Dept. of Homeland if your passport falls under the [Waiver Program](https://www.dhs.gov/visa-waiver-program-requirements)

**Note: Even if you already have a visa or an ESTA for the US, it's worth taking 
a document proving you are entering the US to attend Contribute in case you are 
asked for this at the border. You can request an invitation letter from GitLab
by [filling out this form](https://docs.google.com/forms/d/1hqwakBUfV5uybwtYHnRvkT_ZTm1CEUKqdNmU6e-BPdQ/viewform?edit_requested=true).**

#### ESTA

If your passport country falls under the Visa Waiver program (ESTA) you can apply for that on their site: https://esta.cbp.dhs.gov/esta/
Make sure to read the information **carefully**

For employment information:

```
GitLab Inc
268 Bush Street #350
San Francisco,CA 94104
United States of America 

```

UK employer address:

```
GitLab UK Ltd
Highlands House
Basingstoke Road
Spencers Wood
Reading
Berkshire 
RG7 1NT
```

GitLab BV (NL) employer address:

```
Address line 1: Ondiep 108
Address line 2: Usually empty, unless there aren't dedicated fields for "postal code" and "city"
Postal code: 3552EK
City: Utrecht
Province/State/Region: Usually empty, but if you have to pick something, go with "Utrecht"
```

Detail from https://about.gitlab.com/company/visiting/

For U.S point of contact information:

```
Kirsten Abma
601 LOYOLA AVE
NEW ORLEANS
LOUSIANA
<see Slack profile for contact number>
```

#### Visa

If your passport's country doesn't fall under the waiver program, you need to apply for a visa.
[Please read this page carefully](https://travel.state.gov/content/travel/en/us-visas/tourism-visit/visitor.html)

Reach out to Brittany on the PeopleOps team for assistance in documentation and legal aid

You will need:
* An invitation letter - fill out the form [linked in the handbook](https://about.gitlab.com/handbook/people-operations/visas/#new-orleans-summit-2019-visa-invitation-letter)
* Online Nonimmigrant Visa Application; the [D-160 form](https://ceac.state.gov/genniv/)
* Digitally an up to date passport photo, [requirements for the photo explained here](https://travel.state.gov/content/travel/en/us-visas/visa-information-resources/photos.html)
* An appointment with the US Embassy or Consulate in your country
    * Bring the D-160 form
    * Bring the payment receipt for yourr visa payment
    * Photo when the digital photo upload failed (same requirements as stated above)
    * Invitation letter from GitLab
    
You might require more documentation to show you are planning to leave the US after the conference. More info can be found on [their website](https://travel.state.gov/content/travel/en/us-visas/tourism-visit/visitor.html)


