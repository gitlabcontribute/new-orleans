Yo dawg, I heard you like FAQs, so this is a page devoted to the most F.A.Q.s from
the [FAQs issue](https://gitlab.com/gitlabcontribute/new-orleans/issues/20) and
#contribute2019.

**Note: All permissable expenses are outlined in [the expense policy](https://gitlab.com/gitlabcontribute/new-orleans/blob/master/Contribute_Expense_Policy.md),
which should answer a lot of your questions.**

## General

### I have a new team member starting close to Contribute. Can they still come?

Yes, as soon as they have access to their GitLab email address on Day 1 they will be sent an invitation. 
There is no early access due to legal reasons.
They can download the app when its live and see the details without their account having to be active. 
They can book travel before their start date and expense that on Day 1.

Up until May 5th they will have one week to decide and make their arrangements. They will still
be able to bring their SOs if they choose to buy them a ticket too.

## Travel

For any travel questions after a confirmed booking from NexTravel - please reach out to their support.

### Do I have to use NexTravel to book my flights?

It's preferred, but not mandatory. If you find a cheaper flight combination elsewhere
feel free to go ahead and book it and expense the cost. See [this issue](https://gitlab.com/gitlabcontribute/new-orleans/issues/32)
for more.

### I booked using NexTravel, and have just discovered it didn't include checked baggage. What do I do?

When you check in online 24 hours before you fly, you should be able to add a checked bag
for less than if you do this at the airport. You may expense this cost. Please see
[the expense policy for all Contribute-related expenses](https://gitlab.com/gitlabcontribute/new-orleans/blob/master/Contribute_Expense_Policy.md).

### I booked using NexTravel and need to change my flight. What do I do?

Please reach out to NexTravel support directly for any changes to your booking.

### Entering the United States - Purpose of Trip - don't say 'work'!

When entering the US you'll be asked about 'Purpose of trip' - It's **important not to say the word 'work'!**
That can land you in hot water as it implies you may be earning money or planning to stay.

Recommended answer is **"Attending a conference"**

(It's also OK to say 'business trip'.)

It is wise to take a document proving your intention to attend Contribute – 
we will send out a confirmation email to all attendees before the event, which you 
can present at the border. 

### What about traveling to/from the airport?

You can expense these as general travel costs (see https://about.gitlab.com/handbook/spending-company-money/).
Please use your best judgment regarding what is the most frugal option – 
if it's cheaper to drive and park your car at the airport for a week than get public transport
or a taxi, then do that. Max amounts apply, outlined in the expense [issue](https://gitlab.com/gitlabcontribute/new-orleans/issues/32)

### What about getting to/from the hotel in New Orleans?

We will arrange transfers for all team members between MSY airport and the hotel for May 8th (arrival day) and May 14th (departure day).
**Make sure you added your flight information in CVent!**

### What if my arrival/departure day is different to the official dates (May 8 and 14)?

You will need to arrange your own airport transfers at your own expense.

### I'd like to meet up with other GitLabbers traveling on the same flight. Will you share everyone's flight details?

Yes! We will share a document with all the details by May 2nd.

## Hotel

### I requested a private room. How do I pay?

We haven't asked for payment yet – please keep an eye out for more information to come.

### If I get a private room, can my SO stay in the room without buying a ticket?

Unfortunately not, all SOs need to have a ticket for Contribute.

### I'm sharing a room, but don't have a specific roomie to request. Who will I be sharing with?

You will be randomly allocated a roomie of the same gender. We will share the rooming list by May 2nd.

## Activities

### What's the deal with the team dinners? How do we organize them?

We already sent out an email to leadership, asking them to fill out a form
and give us a headcount for their team dinner. We will distribute pre-loaded Visa cards
of $65 per head (incl SOs) to managers upon arrival on May 8th. It is up to managers to
make a reservation for dinner. 

### But I'm a manager and didn't see anything?

Check with your manager, they might have gotten the info/request. If they didn't check with their manager, etc. etc

## Attire/packing

Please see the [packing list](https://gitlab.com/gitlabcontribute/new-orleans/blob/master/packing-list.md) for specifics.