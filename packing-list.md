# New Orleans suggested items to pack

We'll get a packing list started in this file. A few things to note:

## GitLab Offsite dinner

We'll be outside so make sure to bring a jacket or sweater in case it gets cold. The ground will be uneven so keep that in mind — we don't recommend heels.
You're welcome to wear anything you feel comfortable here, no dresscode otherwise.

## Activities

* GitLab Fest is outdoors in a park near the water. Athletic attire is not required, but we do suggest a hat, sunscreen, and comfortable shoes.
* Please review your excursions and plan your attire accordingly (e.g. comfortable shoes for walking tours, casual clothes for cooking class).
* For your team dinner, please check with the restaurant if there's a dress code. Some places require jackets for men.
* The final party is a Masquerade and can be as fancy or casual as you'd like.

## General restaurant suggestions

Tips for dinner in New Orleans, like the team dinner or going out for drinks.

### Men

* No shorts
* Minimal T-shirt, some a polo or dress shirt (no tank tops)
* No white sneakers/tennis shoes. If you bring dark (tennis) shoes you should be fine

### Women

* For the offsite dinner on Friday - heels are discouraged as the ground will be uneven (gravel and grass)
* Streets are not always a clear path to walk on, so closed toes shoes are recommended

## Other items to consider packing

* Hat (wide brimmed is better)
* Sunglasses (polarized is better, especially near areas with water or glass)
  * [Walgreens and other local pharmacies may sell sunglasses that wrap around the eyes and can go over normal glasses for just shy of $20 USD](https://www.walgreens.com/store/c/solar-shield-fits-over-classic-polarized-plastic-sunglasses-size-l/ID=prod2875476-product)
* Sunscreen (enough to re-apply every 30-45 minutes while outside)
* Mosquito repellent (esp for the Pontoon Swamp excursion)
* Small day backpack
* Items to Fight Heat and Sweat
  * High Humidity will prevent sweat from evaporating
  * Wool (summer / lightweight variety) or Linen clothing absorbs water and are the best to keep dry, at the minimum go for breathable fabrics
  * As a local of Gulf Coast, we never wear wool this time of year. Find a [Columbia fishing shirt](https://www.columbia.com/bahama-ii-s-s-shirt-%7C-455-%7C-xl-191454826879.html?ef_id=EAIaIQobChMIqq7s5NX94QIVFP7jBx0qLQ04EAQYAyABEgJm3fD_BwE:G:s&mid=paidsearch&eid=Google+PLA+US&s_kwcid=AL!3937!3!206821708513!!!g!672827164949!&gclid=EAIaIQobChMIqq7s5NX94QIVFP7jBx0qLQ04EAQYAyABEgJm3fD_BwE) or pants for ventilation.
  Long sleeves and long pants in lighter colors help reflect the sun
  * Cornstarch (keep areas prone to sweat dry preventing rashes)
* Heavy air conditioning means outside/inside temperatures will be wildly different, bring a pullover or jacket

## From 2018 SA Summit...

### For flights

* Passport  
* Cash/currency  
* Boarding pass (mobile/print)  
* Insurance cards and travel insurance paper  
* Credit card without currency conversion fee  
* Neck pillow  
* Ear plugs  
* Eye mask (eye mask and ear plugs handy for roommate cohabitation too)
* Earphones (+batteries)  
* Ereader  
* Portable charger for phone/ereader/other device  
* Compression Socks  
* Reading/book light  
* Activities/entertainment: Books, music  
* Tissues  
* Snacks  
* Sleep aid/meds  
* Hand sanitizer and/orWet wipes

### At Contribute

* Laptop
* Laptop charger
* External storage (if needed)
* Cell phone
* Cell phone chargers & external battery
* Outlet Adaptor / Power converter
* Camera, Camera charger, flash drives, external storage (if you are a photo bug)
* Flashlight
* Tissues
* Rain ponchos or umbrella
* Snacks, any special food
* Medications (enough for the trip)
* "Off" or similar deet bug wipes (if adventuring out of the city or eating in outdoor patios)
* Compact Power Strip (use one converter plug, power existing things without a need for converters on all of them)

### Toiletries

* Contacts, case, glasses, solutions  
* Shampoo, conditioner  
* Makeup, makeup remover  
* Lotion  
* Chapstick  
* Toothbrushes and paste  
* Hair spray  
* Brushes, combs  
* Tylenol, Band-aids for blisters  
* Razors, shaving cream
* Deodorant  
* Dental Floss

### Clothes

* Underwear  
* Bras  
* Socks  
* Pajamas  
* shoes/boots/flipflops  
* Jeans  
* Shirts  
* Light jacket  
* Swimsuit? (hot tub?)  
* Walking shoes  
* Sport clothes (gym at hotel)  

### Other stuff

* Sun hat  
* Sunglasses  
* Fanny pack or small backpack  
* Collapsible checkable bag or extra space in luggage to bring home swag  
* Costume for final night party (Masquerade)

## To Do before leave

* Clean out purse, wallet  
* Pay bills  
* Stop mail  
* Take out the trash  
* Set thermostat for away  
* Call credit card companies to let them know which countries you will be in and which dates to prevent fraud lockdown of credit cards.  
* Water plants  
* Animal care  
* Print/download full itinerary  
* Share itinerary with family/friends as needed  
* Apply Frequent Flyer # (if not already)  
* Cell phone work there? Modify plan for roaming if needed  
* Download Google Map of locations for offline use on Android or iPhone  
* Enroll in Smart Traveler Program @ www.state.gov/travel if in the US
* Scan a copy of your passport and leave it online in case yours gets lost; carry a paper copy with you around town.  

Feel free to keep the list going!